<?php
error_reporting(E_ERROR | E_PARSE);
require_once __DIR__ . '/../vendor/autoload.php';

use FeedIo\Adapter\NullClient;
use FeedIo\Feed;
use FeedIo\FeedIo;
use FeedIo\Parser\XmlParser;
use FeedIo\Reader;
use FeedIo\Reader\Document;
use FeedIo\Rule\DateTimeBuilder;
use FeedIo\Standard\Atom;
use FeedIo\Standard\Rss;
use Psr\Log\NullLogger;

class API
{

    private $url;
    private $since;
    private $feed;

    public function __construct($url, $since)
    {
        $this->url = $url;
        $this->since = $since;
    }

    private function handleUrlScheme()
    {
        $with_protocol = preg_match('/^http/i', $this->url);

        if (!$with_protocol) {
            $this->url = 'https://' . $this->url;
        }
    }

    public function discoverUrl()
    {
        $client = new \FeedIo\Adapter\Http\Client(new Symfony\Component\HttpClient\HttplugClient());
        $feedIo = new FeedIo($client);

        $this->handleUrlScheme();
        $url = $feedIo->discover($this->url);
        if (count($url) && $url[0] !== $this->url) {
            $this->url = rtrim($this->url, '/') . $url[0];
        }
        return $this->url;
    }

    private function fetch()
    {
        $string = file_get_contents($this->url);

        $client = new NullClient();
        $logger = new NullLogger();

        $reader = new Reader($client, $logger);
        $document = new Document($string);

        $standard = $this->getStandard($string);
        $reader->addParser(new XmlParser($standard, $logger));
        $this->feed = $reader->parseDocument($document, new Feed());
    }

    public function getStandard($string)
    {
        try {
            $xml = new SimpleXMLElement($string, LIBXML_NOWARNING | LIBXML_NOERROR | LIBXML_NOCDATA);
        } catch (\Throwable $th) {
            $str = str_replace("\n", '', $string);
            throw new Exception('Cannot parse as XML for string "'. substr($str, 0, 150) .'..."');
        }

        if ($xml->channel) {
            $standard = new Rss(new DateTimeBuilder());
        } else {
            if (
                !in_array('http://www.w3.org/2005/Atom', $xml->getDocNamespaces(), true)
                && !in_array('http://purl.org/atom/ns#', $xml->getDocNamespaces(), true)
            ) {
                throw new Exception('Invalid feed.');
            }
            $standard = new Atom(new DateTimeBuilder());
        }
        return $standard;
    }

    private function getChannel()
    {
        return [
            'url' => $this->url,
            'title' => $this->feed->getTitle(),
            'description' => $this->feed->getDescription(),
        ];
    }

    private function getItems()
    {
        $items = [];

        foreach ($this->feed as $item) {
            array_push($items, $item->toArray());
        }

        // modify each item object property
        $items = array_map(function ($item) {
            $item['url'] = $item['link'];
            unset($item['categories']);
            unset($item['publicId']);
            unset($item['host']);
            unset($item['elements']);
            unset($item['medias']);
            unset($item['link']);
            return $item;
        }, $items);

        // filter by given date at $since
        if (!empty($this->since)) {
            $items = array_filter($items, function ($item) {
                $since = new DateTime($this->since);
                $last_modified = new DateTime($item['lastModified']);
                return !!($last_modified >= $since);
            });
            $items = array_values($items);
        }

        return $items;
    }

    public function getResult($pretty = false)
    {
        $this->fetch();

        $feed = [
            'channel' => $this->getChannel(),
            'items' => $this->getItems(),
        ];

        return json_encode(
            $feed,
            $pretty ? JSON_PRETTY_PRINT : 0
        );
    }
}


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

$api = new API($_GET['url'], $_GET['since']);

try {
    if (isset($_GET['discover'])) echo $api->discoverUrl();
    else echo $api->getResult();
} catch (\Throwable $th) {
    http_response_code(500);
    echo $th->getMessage();
}
